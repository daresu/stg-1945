use crate::{TIME_STEP, WIN_Y};
use bevy::prelude::*;

/// 自機の弾情報
pub struct Shot {
    speed: f32,
    pub hit_area: f32,
}
/// 弾の発射イベント
pub struct NewShotEvent {
    pub player_pos: Vec3,
}

/// 弾の初期化
pub fn shot_init_system(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    mut new_shot_events_reader: EventReader<NewShotEvent>,
) {
    for my_event in new_shot_events_reader.iter() {
        let shot_handle = asset_server.load("images/shot.png");
        let mut shot_pos1 = my_event.player_pos.clone();
        shot_pos1.x -= 17.;
        shot_pos1.y += 17.;
        shot_pos1.z = 9.;

        commands
            .spawn()
            .insert_bundle(SpriteBundle {
                material: materials.add(shot_handle.clone().into()),
                transform: Transform {
                    translation: shot_pos1,
                    ..Default::default()
                },
                ..Default::default()
            })
            .insert(Shot {
                speed: 900.,
                hit_area: 5.,
            });

        let mut shot_pos2 = my_event.player_pos.clone();
        shot_pos2.x += 17.;
        shot_pos2.y += 17.;
        shot_pos2.z = 9.;

        commands
            .spawn()
            .insert_bundle(SpriteBundle {
                material: materials.add(shot_handle.clone().into()),
                transform: Transform {
                    translation: shot_pos2,
                    ..Default::default()
                },
                ..Default::default()
            })
            .insert(Shot {
                speed: 900.,
                hit_area: 5.,
            });
    }
}

/// 弾の移動処理
pub fn shot_move_system(mut commands: Commands, mut query: Query<(&Shot, &mut Transform, Entity)>) {
    for (shot, mut transform, ent) in query.iter_mut() {
        // 移動処理
        transform.translation.y += shot.speed * TIME_STEP;

        // 画面外まで移動したら削除する
        if transform.translation.y >= WIN_Y as f32 {
            commands.entity(ent).despawn();
        }
    }
}
