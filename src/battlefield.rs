use bevy::{app::Events, prelude::*};
use rand::Rng;

use crate::{enemy::NewEnemyEvent, TIME_STEP, WIN_X, WIN_Y};

/// 敵の出現頻度
const ENEMY_PROB: u32 = 13;

/// 背景の情報
pub struct Ocean {
    speed: f32,
}

/// 背景を初期化
pub fn battlefield_init_system(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    let texture_handle = asset_server.load("images/ocean_tile.png");
    let tile_size = Vec2::splat(32.);
    let w = WIN_X / tile_size.x as i32 + 1;
    let h = WIN_Y / tile_size.y as i32 + 2;

    // 背景を敷き詰める
    for y in 0..h {
        for x in 0..w {
            let position = Vec2::new(x as f32, y as f32);
            let translation = (position * tile_size).extend(0.);

            commands
                .spawn()
                .insert_bundle(SpriteBundle {
                    material: materials.add(texture_handle.clone().into()),
                    transform: Transform {
                        translation: translation,
                        ..Default::default()
                    },
                    ..Default::default()
                })
                .insert(Ocean { speed: 25. });
        }
    }
}

/// 背景の移動処理
pub fn battlefield_move_system(mut query: Query<(&Ocean, &mut Transform)>) {
    let tile_size = Vec2::splat(32.);
    let h = WIN_Y / tile_size.y as i32 + 1;

    for (ocean, mut transform) in query.iter_mut() {
        // 移動処理
        transform.translation.y -= (ocean.speed * TIME_STEP).ceil();

        // 画面外まで移動したら削除する
        if transform.translation.y <= -32 as f32 {
            transform.translation.y = h as f32 * tile_size.y
        }
    }
}

/// 敵の出現処理
pub fn battlefield_update_system(mut new_enemy_events: ResMut<Events<NewEnemyEvent>>) {
    if rand::thread_rng().gen_range(0..ENEMY_PROB) == 0 {
        new_enemy_events.send(NewEnemyEvent);
    }
}
