use bevy::{app::Events, prelude::*};

use crate::{
    bomb::Bomb,
    enemy::Enemy,
    plane::{Life, Plane},
    shot::Shot,
    GameData, GameState,
};

/// ライフダウンイベント
pub struct DownLifeEvent;

/// 円と円の当たり判定
/// * `circle1` - 円の1つ目
/// * `circle2` - 円の2つ目
/// * `r1` - 円の1つ目の半径
/// * `r2` - 円の2つ目の半径
pub fn collision_circle(circle1: Vec3, circle2: Vec3, r1: f32, r2: f32) -> bool {
    let x = circle1.x - circle2.x;
    let y = circle1.y - circle2.y;
    let r = r1 + r2;
    if (x * x + y * y) <= (r * r) {
        return true;
    }
    return false;
}

/// 自機と敵の当たり判定
pub fn plane_collision_system(
    mut commands: Commands,
    plane_query: Query<(&Plane, &Transform)>,
    enemy_query: Query<(&Enemy, &Transform, Entity)>,
    mut down_life_events: ResMut<Events<DownLifeEvent>>,
) {
    for (plane, plane_transform) in plane_query.iter() {
        let plane_hit = plane_transform.translation;
        for (enemy, enemy_transform, enemy_ent) in enemy_query.iter() {
            let enemy_hit = enemy_transform.translation;

            // 円と円の当たり判定
            if collision_circle(plane_hit, enemy_hit, plane.hit_area, enemy.hit_area) {
                commands.entity(enemy_ent).despawn();
                down_life_events.send(DownLifeEvent);
            }
        }
    }
}

/// 弾と敵の当たり判定
pub fn shot_collision_system(
    mut commands: Commands,
    shot_query: Query<(&Shot, &Transform, Entity)>,
    enemy_query: Query<(&Enemy, &Transform, Entity)>,
    mut game_data: ResMut<GameData>,
) {
    for (shot, shot_transform, shot_ent) in shot_query.iter() {
        let shot_hit = shot_transform.translation;
        for (enemy, enemy_transform, enemy_ent) in enemy_query.iter() {
            let enemy_hit = enemy_transform.translation;

            // 円と円の当たり判定
            if collision_circle(shot_hit, enemy_hit, shot.hit_area, enemy.hit_area) {
                commands.entity(shot_ent).despawn();
                commands.entity(enemy_ent).despawn();
                game_data.score += 10;
            }
        }
    }
}

/// 弾と自機の当たり判定
pub fn bomb_collision_system(
    mut commands: Commands,
    bomb_query: Query<(&Bomb, &Transform, Entity)>,
    plane_query: Query<(&Plane, &Transform)>,
    mut down_life_events: ResMut<Events<DownLifeEvent>>,
) {
    for (bomb, bomb_transform, bomb_ent) in bomb_query.iter() {
        let bomb_hit = bomb_transform.translation;
        for (plane, plane_transform) in plane_query.iter() {
            let plane_hit = plane_transform.translation;

            // 円と円の当たり判定
            if collision_circle(bomb_hit, plane_hit, bomb.hit_area, plane.hit_area) {
                commands.entity(bomb_ent).despawn();
                down_life_events.send(DownLifeEvent);
            }
        }
    }
}

/// 残機を減らすイベント
pub fn life_down_system(
    mut commands: Commands,
    life_query: Query<(&Life, Entity)>,
    mut state: ResMut<State<GameState>>,
    mut game_data: ResMut<GameData>,
    mut down_life_events_reader: EventReader<DownLifeEvent>,
) {
    for _my_event in down_life_events_reader.iter() {
        if game_data.life > 0 {
            game_data.life -= 1;
            println!("ライフ：{}", game_data.life);
            for (life, life_ent) in life_query.iter() {
                if life.id >= game_data.life {
                    commands.entity(life_ent).despawn();
                }
            }

            if game_data.life == 0 {
                // ゲームオーバー
                println!("ゲームオーバー");
                state.set(GameState::GameOver).unwrap();
            }
        }
    }
}
