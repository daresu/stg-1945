use battlefield::{battlefield_init_system, battlefield_move_system, battlefield_update_system};
use bevy::{
    core::FixedTimestep,
    diagnostic::{FrameTimeDiagnosticsPlugin, LogDiagnosticsPlugin},
    ecs::schedule::ShouldRun,
    prelude::*,
    render::camera::Camera,
    sprite::SpriteSettings,
};
use bomb::{bomb_init_system, bomb_move_system, NewBombEvent};
use collision::{
    bomb_collision_system, life_down_system, plane_collision_system, shot_collision_system,
    DownLifeEvent,
};
use enemy::{
    enemy_animate_sprite_system, enemy_bomb_system, enemy_init_system, enemy_move_system,
    NewEnemyEvent,
};
use plane::{plane_animate_sprite_system, plane_init_system, plane_move_system};
use score::{score_draw_system, score_init_system};
use shot::{shot_init_system, shot_move_system, NewShotEvent};
use title::{title_setup, title_update_system};

mod battlefield;
mod bomb;
mod collision;
mod enemy;
mod plane;
mod score;
mod shot;
mod title;

const TIME_STEP: f32 = 1.0 / 60.0;
/// 画面幅
const WIN_X: i32 = 640;
/// 画面高さ
const WIN_Y: i32 = 480;
const WIN_X_HALF: i32 = WIN_X / 2;
const WIN_Y_HALF: i32 = WIN_Y / 2;

/// 残機の最大数
const LIFE_MAX: u32 = 3;

/// ゲーム状態
#[derive(Clone, Eq, PartialEq, Debug, Hash)]
pub enum GameState {
    Title,
    Playing,
    GameOver,
}

/// ゲームデータ
#[derive(Default)]
pub struct GameData {
    score: u32,
    life: u32,
}

/// メイン関数
fn main() {
    App::build()
        .insert_resource(WindowDescriptor {
            title: "1945".to_string(),
            width: WIN_X as f32,
            height: WIN_Y as f32,
            vsync: true,
            resizable: false,
            cursor_visible: false,
            ..Default::default()
        })
        .insert_resource(ClearColor(Color::rgb(0., 0., 1.)))
        .init_resource::<GameData>()
        .insert_resource(SpriteSettings {
            // NOTE: this is an experimental feature that doesn't work in all cases
            frustum_culling_enabled: true,
        })
        .add_plugins(DefaultPlugins)
        .add_plugin(FrameTimeDiagnosticsPlugin::default())
        .add_plugin(LogDiagnosticsPlugin::default())
        .add_state(GameState::Title)
        .add_event::<NewShotEvent>()
        .add_event::<NewEnemyEvent>()
        .add_event::<NewBombEvent>()
        .add_event::<DownLifeEvent>()
        .add_startup_system(camera_setup.system())
        .add_system_set(
            SystemSet::on_enter(GameState::Title)
                .with_system(title_setup.system())
                .with_system(battlefield_init_system.system()),
        )
        .add_system_set(
            SystemSet::new()
                .with_run_criteria(
                    FixedTimestep::step(TIME_STEP as f64).chain(
                        (|In(input): In<ShouldRun>, state: Res<State<GameState>>| {
                            if state.current() == &GameState::Title {
                                input
                            } else {
                                ShouldRun::No
                            }
                        })
                        .system(),
                    ),
                )
                .with_system(title_update_system.system())
                .with_system(battlefield_move_system.system())
                .with_system(battlefield_update_system.system())
                .with_system(enemy_animate_sprite_system.system()),
        )
        .add_system_set(SystemSet::on_exit(GameState::Title).with_system(teardown.system()))
        .add_system_set(
            SystemSet::on_enter(GameState::Playing)
                .with_system(gamestart_setup.system())
                .with_system(plane_init_system.system())
                .with_system(battlefield_init_system.system())
                .with_system(score_init_system.system()),
        )
        .add_system_set(
            SystemSet::new()
                .with_run_criteria(
                    FixedTimestep::step(TIME_STEP as f64).chain(
                        (|In(input): In<ShouldRun>, state: Res<State<GameState>>| {
                            if state.current() == &GameState::Playing {
                                input
                            } else {
                                ShouldRun::No
                            }
                        })
                        .system(),
                    ),
                )
                .with_system(battlefield_move_system.system())
                .with_system(battlefield_update_system.system())
                .with_system(plane_move_system.system())
                .with_system(shot_init_system.system())
                .with_system(shot_move_system.system())
                .with_system(enemy_init_system.system())
                .with_system(enemy_move_system.system())
                .with_system(enemy_bomb_system.system())
                .with_system(bomb_init_system.system())
                .with_system(bomb_move_system.system())
                .with_system(plane_collision_system.system())
                .with_system(shot_collision_system.system())
                .with_system(bomb_collision_system.system())
                .with_system(life_down_system.system())
                .with_system(score_draw_system.system())
                .with_system(plane_animate_sprite_system.system())
                .with_system(enemy_animate_sprite_system.system()),
        )
        .add_system_set(
            SystemSet::on_enter(GameState::GameOver).with_system(gameover_setup.system()),
        )
        .add_system_set(
            SystemSet::new()
                .with_run_criteria(
                    FixedTimestep::step(TIME_STEP as f64).chain(
                        (|In(input): In<ShouldRun>, state: Res<State<GameState>>| {
                            if state.current() == &GameState::GameOver {
                                input
                            } else {
                                ShouldRun::No
                            }
                        })
                        .system(),
                    ),
                )
                .with_system(battlefield_move_system.system())
                .with_system(battlefield_update_system.system())
                .with_system(shot_move_system.system())
                .with_system(enemy_init_system.system())
                .with_system(enemy_move_system.system())
                .with_system(bomb_move_system.system())
                .with_system(gameover_keyboard.system())
                .with_system(score_draw_system.system())
                .with_system(enemy_animate_sprite_system.system()),
        )
        .add_system_set(SystemSet::on_exit(GameState::GameOver).with_system(teardown.system()))
        .add_system(bevy::input::system::exit_on_esc_system.system())
        .run();
}

/// 初期セットアップ
fn camera_setup(mut commands: Commands, mut game_data: ResMut<GameData>) {
    let camera_pos = Vec3::new(WIN_X_HALF as f32, WIN_Y_HALF as f32, 999.9);

    // カメラ
    commands.spawn_bundle(OrthographicCameraBundle {
        transform: Transform::from_translation(camera_pos),
        ..OrthographicCameraBundle::new_2d()
    });

    game_data.score = 0;
    game_data.life = LIFE_MAX;
}

/// 画面遷移時にクリアする
fn teardown(mut commands: Commands, entities: Query<Entity, Without<Camera>>) {
    for entity in entities.iter() {
        commands.entity(entity).despawn_recursive();
    }
}

/// ゲーム開始時の初期化
fn gamestart_setup(mut game_data: ResMut<GameData>) {
    println!("ゲームスタート");
    game_data.score = 0;
    game_data.life = LIFE_MAX;
}

/// ゲームオーバーのセットアップ
fn gameover_setup(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    println!("ゲームオーバー");
    // ゲームオーバー
    let texture_handle = asset_server.load("images/gameover.png");
    commands.spawn_bundle(SpriteBundle {
        material: materials.add(texture_handle.into()),
        transform: Transform {
            translation: Vec3::new(
                WIN_X_HALF as f32,
                (WIN_Y_HALF + WIN_Y_HALF / 2) as f32,
                900.,
            ),
            ..Default::default()
        },
        ..Default::default()
    });
}

/// ゲームオーバー時の操作
fn gameover_keyboard(mut state: ResMut<State<GameState>>, keyboard_input: Res<Input<KeyCode>>) {
    if keyboard_input.just_pressed(KeyCode::Space) {
        state.set(GameState::Title).unwrap();
    }
}
