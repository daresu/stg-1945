use bevy::prelude::*;

use crate::{GameState, WIN_X_HALF, WIN_Y_HALF};

/// タイトルのカーソル情報
pub struct Title {
    pos: u32,
    sleep: u32,
}

/// タイトルのセットアップ
pub fn title_setup(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    println!("タイトル");

    // タイトル
    let texture_handle = asset_server.load("images/title.png");
    commands.spawn_bundle(SpriteBundle {
        material: materials.add(texture_handle.into()),
        transform: Transform {
            translation: Vec3::new(
                WIN_X_HALF as f32,
                (WIN_Y_HALF + WIN_Y_HALF / 2) as f32,
                900.,
            ),
            ..Default::default()
        },
        ..Default::default()
    });

    // Play
    let texture_handle = asset_server.load("images/title_play.png");
    commands.spawn_bundle(SpriteBundle {
        material: materials.add(texture_handle.into()),
        transform: Transform {
            translation: Vec3::new(WIN_X_HALF as f32, (WIN_Y_HALF - 100) as f32, 900.),
            ..Default::default()
        },
        ..Default::default()
    });

    // Quit
    let texture_handle = asset_server.load("images/title_quit.png");
    commands.spawn_bundle(SpriteBundle {
        material: materials.add(texture_handle.into()),
        transform: Transform {
            translation: Vec3::new(WIN_X_HALF as f32, (WIN_Y_HALF - 150) as f32, 900.),
            ..Default::default()
        },
        ..Default::default()
    });

    // カーソル
    let texture_handle = asset_server.load("images/title_cur.png");
    commands
        .spawn_bundle(SpriteBundle {
            material: materials.add(texture_handle.into()),
            transform: Transform {
                translation: Vec3::new((WIN_X_HALF - 52) as f32, (WIN_Y_HALF - 99) as f32, 900.),
                ..Default::default()
            },
            ..Default::default()
        })
        .insert(Title { pos: 0, sleep: 10 });
}

/// タイトルのカーソル操作
pub fn title_update_system(
    keyboard_input: Res<Input<KeyCode>>,
    mut query: Query<(&mut Title, &mut Transform)>,
    mut state: ResMut<State<GameState>>,
) {
    if let Ok((mut title, mut transform)) = query.single_mut() {
        if keyboard_input.just_pressed(KeyCode::Up) || keyboard_input.just_pressed(KeyCode::W) {
            if title.pos == 1 {
                title.pos = 0;
            }
        }

        if keyboard_input.just_pressed(KeyCode::Down) || keyboard_input.just_pressed(KeyCode::S) {
            if title.pos == 0 {
                title.pos = 1;
            }
        }

        let translation = &mut transform.translation;
        if title.pos == 0 {
            translation.y = (WIN_Y_HALF - 99) as f32;
        } else {
            translation.y = (WIN_Y_HALF - 149) as f32;
        }

        if keyboard_input.just_pressed(KeyCode::Space) {
            if title.sleep <= 0 {
                if title.pos == 0 {
                    state.set(GameState::Playing).unwrap();
                } else {
                    std::process::exit(0);
                }
            }
        }

        if title.sleep > 0 {
            title.sleep -= 1;
        }
    }
}
