use crate::{NewShotEvent, LIFE_MAX, TIME_STEP, WIN_X, WIN_X_HALF, WIN_Y};
use bevy::{app::Events, prelude::*};

/// 自機情報
pub struct Plane {
    speed: f32,
    ani_cnt: u32,
    reload_time: u32,
    pub hit_area: f32,
}

/// 残機情報
pub struct Life {
    pub id: u32,
}

/// 自機の初期処理
pub fn plane_init_system(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut texture_atlases: ResMut<Assets<TextureAtlas>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    // 自機の読み込み
    let texture_handle = asset_server.load("images/plane.png");
    let texture_atlas = TextureAtlas::from_grid(texture_handle, Vec2::splat(65.), 1, 3);
    let texture_atlas_handle = texture_atlases.add(texture_atlas);
    commands
        .spawn()
        .insert_bundle(SpriteSheetBundle {
            texture_atlas: texture_atlas_handle,
            transform: Transform::from_translation(Vec3::new(WIN_X_HALF as f32, 50., 10.)),
            ..Default::default()
        })
        .insert(Timer::from_seconds(0.05, true))
        .insert(Plane {
            speed: 200.,
            ani_cnt: 0,
            reload_time: 0,
            hit_area: 5.,
        });

    // 残機の読み込み
    let texture_handle = asset_server.load("images/plane_life.png");
    for x in 0..LIFE_MAX {
        let translation = Vec3::new((35 + (25 * x)) as f32, 32., 900.);

        commands
            .spawn_bundle(SpriteBundle {
                material: materials.add(texture_handle.clone().into()),
                transform: Transform::from_translation(translation),
                ..Default::default()
            })
            .insert(Life { id: x });
    }
}

/// 自機の移動処理
pub fn plane_move_system(
    keyboard_input: Res<Input<KeyCode>>,
    mut query: Query<(&mut Plane, &mut Transform)>,
    mut new_shot_events: ResMut<Events<NewShotEvent>>,
) {
    if let Ok((mut plane, mut transform)) = query.single_mut() {
        let mut vec = Vec3::new(0., 0., 0.);

        // 移動処理
        if keyboard_input.pressed(KeyCode::Left) || keyboard_input.pressed(KeyCode::A) {
            vec.x = -1.;
        }

        if keyboard_input.pressed(KeyCode::Right) || keyboard_input.pressed(KeyCode::D) {
            vec.x = 1.;
        }

        if keyboard_input.pressed(KeyCode::Up) || keyboard_input.pressed(KeyCode::W) {
            vec.y = 1.;
        }

        if keyboard_input.pressed(KeyCode::Down) || keyboard_input.pressed(KeyCode::S) {
            vec.y = -1.;
        }

        if vec.length() > 0. {
            vec = vec.normalize() * plane.speed * TIME_STEP;
        }
        let translation = &mut transform.translation;
        *translation += vec;

        // 画面外への移動制限
        translation.x = translation.x.min(WIN_X as f32).max(0.);
        translation.y = translation.y.min(WIN_Y as f32).max(0.);

        if keyboard_input.pressed(KeyCode::Space) {
            // リロード時間を考慮する
            if plane.reload_time <= 0 {
                new_shot_events.send(NewShotEvent {
                    player_pos: *translation,
                });
                plane.reload_time = 15;
            }
        }

        if plane.reload_time > 0 {
            plane.reload_time -= 1;
        }
    }
}

/// 自機のアニメーション
pub fn plane_animate_sprite_system(
    time: Res<Time>,
    mut query: Query<(&mut Plane, &mut Timer, &mut TextureAtlasSprite)>,
) {
    for (mut plane, mut timer, mut sprite) in query.iter_mut() {
        timer.tick(time.delta());
        if timer.finished() {
            sprite.index += 1;
            plane.ani_cnt += 1;
            if plane.ani_cnt >= 3 {
                sprite.index -= plane.ani_cnt;
                plane.ani_cnt = 0;
            }
        }
    }
}
