use crate::{TIME_STEP, WIN_X, WIN_Y};
use bevy::prelude::*;

/// 敵の弾情報
pub struct Bomb {
    speed: f32,
    vec: Vec2,
    pub hit_area: f32,
}
/// 弾の発射イベント
pub struct NewBombEvent {
    pub player_pos: Vec3,
    pub enemy_pos: Vec3,
}

/// 弾の初期化
pub fn bomb_init_system(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    mut new_bomb_events_reader: EventReader<NewBombEvent>,
) {
    for my_event in new_bomb_events_reader.iter() {
        let bomb_handle = asset_server.load("images/bomb.png");
        let mut shot_pos = my_event.enemy_pos.clone();
        shot_pos.z = 4.;

        // 自機の方向の角度を計算
        let angle = f32::atan2(
            my_event.player_pos.y - my_event.enemy_pos.y,
            my_event.player_pos.x - my_event.enemy_pos.x,
        );

        commands
            .spawn()
            .insert_bundle(SpriteBundle {
                material: materials.add(bomb_handle.into()),
                transform: Transform {
                    translation: shot_pos,
                    ..Default::default()
                },
                ..Default::default()
            })
            .insert(Bomb {
                speed: 500.,
                vec: Vec2::new(f32::cos(angle), f32::sin(angle)),
                hit_area: 5.,
            });
    }
}

/// 弾の移動処理
pub fn bomb_move_system(mut commands: Commands, mut query: Query<(&Bomb, &mut Transform, Entity)>) {
    for (shot, mut transform, ent) in query.iter_mut() {
        // 移動処理
        transform.translation.x += shot.speed * shot.vec.x * TIME_STEP;
        transform.translation.y += shot.speed * shot.vec.y * TIME_STEP;

        // 画面外まで移動したら削除する
        if transform.translation.y <= 0 as f32
            || transform.translation.y >= WIN_Y as f32
            || transform.translation.x <= 0 as f32
            || transform.translation.x >= WIN_X as f32
        {
            commands.entity(ent).despawn();
        }
    }
}
