use crate::{bomb::NewBombEvent, plane::Plane, TIME_STEP, WIN_X, WIN_Y};
use bevy::{app::Events, prelude::*};
use rand::Rng;

/// 発射の頻度
const SHOT_PROB: u32 = 128;

/// 敵情報
pub struct Enemy {
    speed: f32,
    ani_cnt: u32,
    pub hit_area: f32,
}

/// 敵の出現イベント
pub struct NewEnemyEvent;

/// 敵の初期処理
pub fn enemy_init_system(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut texture_atlases: ResMut<Assets<TextureAtlas>>,
    mut new_enemy_events_reader: EventReader<NewEnemyEvent>,
) {
    for _my_event in new_enemy_events_reader.iter() {
        // 敵の読み込み
        let enemy_handle = asset_server.load("images/enemy.png");
        let texture_atlas = TextureAtlas::from_grid(enemy_handle, Vec2::new(32., 32.), 3, 5);
        let texture_atlas_handle = texture_atlases.add(texture_atlas);
        let pos_x = rand::thread_rng().gen_range(10..WIN_X - 10) as f32;
        let pos_y = (WIN_Y) as f32;
        let enemy_color = rand::thread_rng().gen_range(0..5);
        commands
            .spawn()
            .insert_bundle(SpriteSheetBundle {
                sprite: TextureAtlasSprite::new(enemy_color * 3),
                texture_atlas: texture_atlas_handle,
                transform: Transform::from_translation(Vec3::new(pos_x, pos_y, 5.)),
                ..Default::default()
            })
            .insert(Enemy {
                speed: 200.,
                ani_cnt: 0,
                hit_area: 16.,
            })
            .insert(Timer::from_seconds(0.05, true));
    }
}

/// 敵の移動処理
pub fn enemy_move_system(
    mut commands: Commands,
    mut enemy_query: Query<(&Enemy, &mut Transform, Entity)>,
) {
    for (enemy, mut transform, ent) in enemy_query.iter_mut() {
        transform.translation.y -= enemy.speed * TIME_STEP;

        // 画面外まで移動したら削除する
        if transform.translation.y <= 0 as f32
            || transform.translation.y >= WIN_Y as f32
            || transform.translation.x <= 0 as f32
            || transform.translation.x >= WIN_X as f32
        {
            commands.entity(ent).despawn();
        }
    }
}

/// 敵の弾発射処理
pub fn enemy_bomb_system(
    plane_query: Query<(&Plane, &Transform)>,
    enemy_query: Query<(&Enemy, &Transform)>,
    mut new_bomb_events: ResMut<Events<NewBombEvent>>,
) {
    for (_enemy, enemy_transform) in enemy_query.iter() {
        if rand::thread_rng().gen_range(0..SHOT_PROB) == 0 {
            if let Ok((_plane, plane_transform)) = plane_query.single() {
                new_bomb_events.send(NewBombEvent {
                    player_pos: plane_transform.translation,
                    enemy_pos: enemy_transform.translation,
                });
            }
        }
    }
}

/// 敵のアニメーション
pub fn enemy_animate_sprite_system(
    time: Res<Time>,
    mut query: Query<(&mut Enemy, &mut Timer, &mut TextureAtlasSprite)>,
) {
    for (mut enemy, mut timer, mut sprite) in query.iter_mut() {
        timer.tick(time.delta());
        if timer.finished() {
            sprite.index += 1;
            enemy.ani_cnt += 1;
            if enemy.ani_cnt >= 3 {
                sprite.index -= enemy.ani_cnt;
                enemy.ani_cnt = 0;
            }
        }
    }
}
