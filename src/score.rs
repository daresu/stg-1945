use bevy::prelude::*;

use crate::{GameData, WIN_Y};

pub struct Score;

/// スコアの初期化
pub fn score_init_system(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    // スコアラベル
    let texture_handle = asset_server.load("images/score.png");
    commands.spawn_bundle(SpriteBundle {
        transform: Transform::from_xyz(35., (WIN_Y - 15) as f32, 900.),
        material: materials.add(texture_handle.into()),
        ..Default::default()
    });

    // スコア数字
    let font = asset_server.load("fonts/FiraSans-Bold.ttf");
    let text_style = TextStyle {
        font,
        font_size: 20.,
        color: Color::WHITE,
    };
    let text_alignment = TextAlignment {
        vertical: VerticalAlign::Center,
        horizontal: HorizontalAlign::Right,
    };
    let translation = Vec3::new((35 + 30 + 10) as f32, (WIN_Y - 15) as f32, 900.);
    commands
        .spawn_bundle(Text2dBundle {
            text: Text::with_section("00000", text_style.clone(), text_alignment),
            transform: Transform::from_translation(translation),
            ..Default::default()
        })
        .insert(Score);
}

/// スコアの描画
pub fn score_draw_system(mut query: Query<(&Score, &mut Text)>, game_data: Res<GameData>) {
    let score_num = format!("{:05}", game_data.score);
    if let Ok((_score, mut text)) = query.single_mut() {
        text.sections[0].value = score_num;
    }
}
